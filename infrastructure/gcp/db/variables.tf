variable "project_id" {
  type        = string
  description = "Variável responsável pelo ID do Projeto da GCP."
}

variable "zone" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}


variable "region" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}


variable "network" {
  type        = string
  description = "Valor da rede a ser configurada na instância."
}

#variable "require_ssl" {
#  description = "True if the instance should require SSL/TLS for users connecting over IP. Note: SSL/TLS is needed to provide security when you connect to Cloud SQL using IP addresses. If you are connecting to your instance only by using the Cloud SQL Proxy or the Java Socket Library, you do not need to configure your instance to use SSL/TLS."
#  type        = bool
#  default     = true
#}