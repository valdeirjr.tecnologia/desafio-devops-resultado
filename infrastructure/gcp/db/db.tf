#########################################################################
##Quando só existe a VPC ###
#########################################################################
#}


### Criação de uma nova rede privada " cloud sql" para fazer pering com a VPC ###
resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta
  project = var.project_id

  name          = "database-mysql"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 24
  network       = var.network
  address = "192.168.40.0"
}


### Fazer o peering da VPC com a rede Privada criada a cima
resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network                 = var.network
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}


#########################################################################
##Quando a rede do banco ja existe e ja tem o peering com o cloud sql ###
#########################################################################
resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "instance" {
  provider = google-beta
  project  = var.project_id

  #name = "private-instance-${random_id.db_name_suffix.hex}"
  name             = "inst-database-desafio"
  region              = "us-central1"
  database_version    = "MYSQL_8_0"
  deletion_protection = false


   depends_on = [google_service_networking_connection.private_vpc_connection]

  settings {
    tier      = "db-f1-micro"
    disk_size = 20
    disk_type = "PD_SSD"
    backup_configuration {
      binary_log_enabled             = true
      enabled                        = true
      location                       = "us"
      point_in_time_recovery_enabled = false
      start_time                     = "00:00"
      transaction_log_retention_days = 7

      backup_retention_settings {
        retained_backups = 7
        retention_unit   = "COUNT"
      }
    }
    ip_configuration {
      ipv4_enabled       = false
      private_network    = var.network
      allocated_ip_range = "database-mysql" # Nome do peering da VPC com o banco
    }
  }


 
}

resource "google_sql_database" "database" {
  project   = var.project_id
  name      = "desafio"
  instance  = google_sql_database_instance.instance.name
  charset   = "utf8"
  collation = "utf8_general_ci"
}

resource "google_sql_user" "users" {
  project  = var.project_id
  name     = "dev"
  instance = google_sql_database_instance.instance.name
  host     = "%"
  password = "Temporario"
}


provider "google-beta" {
  region = var.region
  zone   =var.zone
  
}