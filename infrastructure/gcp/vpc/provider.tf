terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.32.0"
    }
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
  credentials = "/projetos/chaves/desafio-devops-394302-b821d35ab84d.json"
}
