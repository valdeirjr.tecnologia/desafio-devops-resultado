resource "google_project_service" "project" {
  project = var.project_id
  for_each  = var.service_api
  service = each.value.service_api_usage

  timeouts {
    create = "30m"
    update = "40m"
  }

  disable_dependent_services = true
}


resource "google_compute_network" "vpc_network" {
  name                            = var.vpc_name
  project                         = var.project_id
  description                     = var.vpc_description
  auto_create_subnetworks         = var.auto_create_subnetworks
  delete_default_routes_on_create = var.delete_default_routes_on_create
  routing_mode                    = var.routing_mode
  mtu                             = var.mtu
  enable_ula_internal_ipv6        = var.vpc_enable_ula_internal_ipv6

}

