variable "project_id" {
  type        = string
  description = "Variável responsável pelo ID do Projeto da GCP."
}

variable "environment" {
  type        = string
  description = "Ambiente que será executado o Terraform"
}

variable "zone" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}

variable "region" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}

variable "machine_type" {
  type        = string
  description = "Variável responsável pelo tipo da instância."
}

variable "source_image" {
  type        = string
  description = "Imagem para ser utilizacada na criação da instância."
}

#variable "network" {
#  type        = string
#  description = "Valor da rede a ser configurada na instância."
#}

#variable "subnetwork" {
#  type        = string
#  description = "Valor a subrede a ser configurada na instância."
#}

variable "disk_size" {
  type        = number
  description = "Valor para o tamanho do disco."
}

variable "disk_type" {
  type        = string
  description = "Valor para o tipo do disco."
}

variable "service_account" {
  type        = string
  description = "Valor da conta padrão de serviço utilizada."
}

variable "name_prefix" {
  type        = string
  description = "Prefixo do nome padrão Hering com padrão para template."

}

variable "script_linux" {
  type        = string
  description = "Caminho do script de config "
  default     = "./script/init.sh"

}

variable "script_chave" {
  type        = string
  description = "Caminho da chave ssh "
  default     = "./chave/id_rsa.pub"

}

#### Geral Globais
variable "build_by" { default = "" }

variable "prefix" { default = "" }


variable "service_api" { default = "" }

### Locais
variable "bu" { default = "" }
variable "bu_id" { default = "" }
variable "criticality" { default = "" }
variable "owner" { default = "" }
variable "product" { default = "" }

variable "scalation_list" { default = "" }

#--------------------------------------------------------------------------------------------------
### 1 Network
#--------------------------------------------------------------------------------------------------

variable "vpc_name" { default = "" }
variable "auto_create_subnetworks" { default = "" }
variable "delete_default_routes_on_create" { default = "" }
variable "mtu" { default = "" }
variable "routing_mode" { default = "" }
variable "vpc_description" { default = "" }
variable "vpc_enable_ula_internal_ipv6" { default = "" }
variable "shared_vpc_config" { default = "" }
variable "subnetwork_config" { default = "" }
variable "subnetwork_gke_nodes_config" { default = "" }
variable "firewall_config" { default = "" }
variable "route_config" { default = "" }
variable "private_cloudsql_config" { default = "" }



### Network ## SERVICE NETWORK CONNECTION:
variable "service_networking_connection" { default = "" }
variable "reserved_peering_ranges" {
  type    = list(any)
  default = []
}

### Network ## PRIVATE SERVICE CONNECTION:

variable "private_cloudsql_range_name"    { default = "" }
variable "private_cloudsql_purpose"       { default = "" }
variable "private_cloudsql_address_type"  { default = "" }
variable "private_cloudsql_prefix_length" { default = "" }
variable "private_cloudsql_address"       { default = "" }


#--------------------------------------------------------------------------------------------------
### 2 SubNetwork
#--------------------------------------------------------------------------------------------------

variable "network" { default = "" }
variable "private_ip_google_access" { default = "" }
variable "aggregation_interval" { default = "" }
variable "flow_sampling" { default = "" }
variable "metadata" { default = "" }

variable "subnet_resources_name"            { default = ""}
variable "subnet_resources_ip_cidr_range"   { default = ""}
variable "subnet_resources_description"     { default = ""}


variable "subnet_nodes_name"            { default = ""}
variable "subnet_nodes_ip_cidr_range"   { default = ""}
variable "subnet_nodes_description"     { default = ""}
variable "subnet_pods_range_name"       { default = ""}
variable "subnet_pods_range_ipaddr"     { default = ""}
variable "subnet_services_range_name"   { default = ""}
variable "subnet_services_range_ipaddr" { default = ""}


#--------------------------------------------------------------------------------------------------
### 4 Firewall
#--------------------------------------------------------------------------------------------------

variable "enable_logging" { default = "" }
variable "sa" { default = "" }

# Rule1: allow-iap-gcp-rdp-ssh
variable "rule_name" { default = "" }
variable "rule_ports" { default = "" }
variable "rule_source_ranges" { default = "" }
variable "rule_target_tags" { default = "" }
variable "rule_direction" { default = "" }
variable "rule_priority_default" { default = "" }
variable "rule_protocol1" { default = "" }
variable "rule_protocol2" { default = "" }

#variable "system_label" {
#  type        = string
#  description = "Objetivo da maquina virtual"
#  default     = "template"
#}

# variable "replicas" {
#   type        = list(string)
#   description = "Replicas Secrets."
#   default     = ["us-east1"]
# }

# variable "secret" {
#   type        = string
#   description = "Valor do secret a ser armazenado no Google."
#   sensitive   = true
# }

# variable "secret_id" {
#   type        = string
#   description = "Identificador do secret."
# }

# variable "secret_manager_viewers" {
#   type        = list(string)
#   description = "Lista dos usuário que podem somente visualizar o secrets."
#   default     = ["serviceAccount:"]
# }