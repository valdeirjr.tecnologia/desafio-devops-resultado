project_id      = "desafio-devops-394302"
environment     = "dev"
region          = "us-central1"
zone            = "us-central1-c"
name_prefix     = "cvm000hrg016"
machine_type    = "n2-custom-4-8192"
disk_type       = "pd-ssd"
disk_size       = "70"
source_image = "projects/debian-cloud/global/images/debian-11-bullseye-v20230306"
#network         = "vpc-desafio-dev"
#subnetwork   = "projects/hering-bifrost/regions/southamerica-east1/subnetworks/hering-bifrost-sa-east1-prod"
service_account = "sa-deploy@desafio-devops-394302.iam.gserviceaccount.com"



#--------------------------------------------------------------------------------------------------
### 1 Network
#--------------------------------------------------------------------------------------------------

### Network Gerais
auto_create_subnetworks         = "false"
delete_default_routes_on_create = "false"
mtu                             = "1460"
routing_mode                    = "REGIONAL"
vpc_description              = "Rede de VPC do desafio"
vpc_name          = "vpc-desafio-dev"
vpc_enable_ula_internal_ipv6 = false
service_api = {
  regra-01 = {
    service_api_usage = "cloudresourcemanager.googleapis.com" 
  }
  
  regra-02 = {
    service_api_usage = "servicenetworking.googleapis.com" 
  }
}




##SERVICE NETWORK CONNECTION:
#service_networking_connection = "servicenetworking.googleapis.com"
#reserved_peering_ranges = [
#  "mysql-private-subnet-dev"
#]

# PRIVATE SERVICE CONNECTION:

#private_cloudsql_config = {
  
 # regra-sql-1 = {
 #   private_cloudsql_range_name    = "mysql-private-subnet-dev"
 #   private_cloudsql_purpose       = "VPC_PEERING"
 #   private_cloudsql_address_type  = "INTERNAL"
 #   private_cloudsql_prefix_length = "24"
 #  private_cloudsql_address       = "10.200.0.0"
 # }
#}



#--------------------------------------------------------------------------------------------------
### 2 SubNetwork
#--------------------------------------------------------------------------------------------------

# VARIÁVEIS FIXAS PARA TODAS SUBNETS DESSA VPC:

/*
CIDR BLOCK Network - 172.24.0.0/14 (Rede utilizada para criação das subnets em CERTIFICAÇÃO)
*/


private_ip_google_access = true
aggregation_interval     = "INTERVAL_5_MIN"
flow_sampling            = "0.5"
metadata                 = "INCLUDE_ALL_METADATA"

### CRIAÇÃO DAS SUBNETS:

# SUBNET PRIVADOS
# OBJETIVO: RESOURCES (GCE)
subnetwork_config = {
  regra-01 = {
    subnet_resources_name            = "subnet-resources-us-central1-prv-01"
    subnet_resources_ip_cidr_range   = "10.120.0.0/24"
    subnet_resources_description     = "Subnet para o range de recursos"
    region                           = "us-central1"   
    
  },
  regra-02 = {
    subnet_resources_name            = "subnet-resources-us-central1-prv-02"
    subnet_resources_ip_cidr_range   = "10.120.1.0/24"
    subnet_resources_description     = "Subnet para o range de recursos"
    region                           = "us-central1"   
    
  },
  regra-03 = {
    subnet_resources_name            = "subnet-resources-us-central1-prv-03"
    subnet_resources_ip_cidr_range   = "10.120.2.0/24"
    subnet_resources_description     = "Subnet para o range de recursos privados"
    region                           = "us-central1"   
    
  }
}



# SUBNET  PUBLIC
# OBJETIVO: GKE (NODES), GKE Secundária (PODs), GKE Secundária (SERVICES)
subnetwork_gke_nodes_config = {
 regra-01 = {
  subnet_nodes_name            = "subnet-gke-desafio-nodes-us-central1"
  subnet_nodes_ip_cidr_range   = "10.2.0.0/24"
  subnet_nodes_description     = "Subnet para o range de nodes do GKE teste"
  subnet_pods_range_name       = "subnet-gke-desafio-pods-us-central1"
  subnet_pods_range_ipaddr     = "10.4.0.0/25"
  subnet_services_range_name   = "subnet-gke-desafio-services-us-us-central1"
  subnet_services_range_ipaddr = "10.1.0.0/27"
   region                      = "us-central1"
 }

}
