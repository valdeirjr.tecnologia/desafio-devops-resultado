variable "project_id" {
  type        = string
  description = "Variável responsável pelo ID do Projeto da GCP."
}

variable "environment" {
  type        = string
  description = "Ambiente que será executado o Terraform"
}

variable "zone" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}

variable "region" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}

variable "network" {
  type        = string
  description = "Valor da rede a ser configurada na instância."
}

variable "metadata" {
  type        = string
  
}
#variable "subnetwork" {
#  type        = string
#  description = "Valor a subrede a ser configurada na instância."
#}
