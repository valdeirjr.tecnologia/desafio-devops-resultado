terraform {
  backend "gcs" {
   
    bucket  = "desafio-devops-bucket-01"
    prefix  = "terraform/tfstate-firewall"
  }
}
