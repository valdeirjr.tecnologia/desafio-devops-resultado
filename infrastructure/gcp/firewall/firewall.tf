
resource "google_compute_firewall" "allow-iap" {
  name                = "allow-iap"
  network             = var.network
  direction           = "INGRESS"
  priority            = "1000"
  source_ranges       = ["35.235.240.0/20"]
  target_tags         = ["allow-iap"]

  allow {
    protocol          = "tcp"
    ports             =  ["22","3389"]
  }

  log_config {
    metadata          = var.metadata
  }
}

resource "google_compute_firewall" "allow-health-checks" {
  name                = "allow-health-checks"
  network             = var.network
  direction           = "INGRESS"
  priority            = "1000"
  source_ranges       = ["35.191.0.0/16","130.211.0.0/22"]
  target_tags         = ["allow-health-checks"]

  allow {
    protocol          = "tcp"
    ports             =  ["80","443","8443", "3000", "8080"]
  }

  log_config {
    metadata          = var.metadata
  }
}


resource "google_compute_firewall" "allow-network-lb-health-checks" {
  name                = "allow-network-lb-health-checks"
  network             = var.network
  direction           = "INGRESS"
  priority            = "1000"
  source_ranges       = ["35.191.0.0/16","209.85.152.0/22","209.85.204.0/22"]
  target_tags         = ["allow-network-lb-health-checks"]

  allow {
    protocol          = "tcp"
    ports             =  ["80","443","8443", "3000", "8080"]
  }

  log_config {
    metadata          = var.metadata
  }
}

resource "google_compute_firewall" "allow-network-aplication" {
  name                = "allow-application"
  network             = var.network
  direction           = "INGRESS"
  priority            = "1000"
  source_ranges       = ["0.0.0.0/0"]
  target_tags         = ["allow-application"]

  allow {
    protocol          = "tcp"
    ports             =  ["80"]
  }

  log_config {
    metadata          = var.metadata
  }
}