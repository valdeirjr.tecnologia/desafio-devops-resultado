variable "project_id" {
  type        = string
  description = "Variável responsável pelo ID do Projeto da GCP."
}

variable "environment" {
  type        = string
  description = "Ambiente que será executado o Terraform"
}

variable "zone" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}

variable "region" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}

variable "network" {
  type        = string
  description = "Valor da rede a ser configurada na instância."
}


variable "service_account" {
  type        = string
  description = "Valor da conta padrão de serviço utilizada."
}

#--------------------------------------------------------------------------------------------------
### 2 SubNetwork
#--------------------------------------------------------------------------------------------------

variable "subnetwork_config" { default = "" }
variable "subnetwork_gke_nodes_config" { default = "" }

variable "private_ip_google_access" { default = "" }
variable "aggregation_interval" { default = "" }
variable "flow_sampling" { default = "" }
variable "metadata" { default = "" }

variable "subnet_resources_name"            { default = ""}
variable "subnet_resources_ip_cidr_range"   { default = ""}
variable "subnet_resources_description"     { default = ""}


variable "subnet_nodes_name"            { default = ""}
variable "subnet_nodes_ip_cidr_range"   { default = ""}
variable "subnet_nodes_description"     { default = ""}
variable "subnet_pods_range_name"       { default = ""}
variable "subnet_pods_range_ipaddr"     { default = ""}
variable "subnet_services_range_name"   { default = ""}
variable "subnet_services_range_ipaddr" { default = ""}

