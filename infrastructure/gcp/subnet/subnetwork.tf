resource "google_compute_subnetwork" "subnet-resources-template" {
  for_each                 = var.subnetwork_config
  description              = each.value.subnet_resources_description
  ip_cidr_range            = each.value.subnet_resources_ip_cidr_range
  name                     = each.value.subnet_resources_name
  network                  = var.network
  private_ip_google_access = var.private_ip_google_access
  project                  = var.project_id
  region                   = each.value.region
  log_config {
    aggregation_interval = var.aggregation_interval
    flow_sampling        = var.flow_sampling
    metadata             = var.metadata
  }
}

resource "google_compute_subnetwork" "subnet-gke-template" {
  for_each                 = var.subnetwork_gke_nodes_config
  description              = each.value.subnet_nodes_description
  ip_cidr_range            = each.value.subnet_nodes_ip_cidr_range
  name                     = each.value.subnet_nodes_name
  network                  = var.network
  private_ip_google_access = var.private_ip_google_access
  project                  = var.project_id
  region                   = each.value.region
  log_config {
    aggregation_interval = var.aggregation_interval
    flow_sampling        = var.flow_sampling
    metadata             = var.metadata
  }
  secondary_ip_range {
    range_name    = each.value.subnet_pods_range_name
    ip_cidr_range = each.value.subnet_pods_range_ipaddr
  }
  secondary_ip_range {
    range_name    = each.value.subnet_services_range_name
    ip_cidr_range = each.value.subnet_services_range_ipaddr
  }

}
