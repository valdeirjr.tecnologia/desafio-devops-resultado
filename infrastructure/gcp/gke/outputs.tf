output "region" {
  value       = var.regiao
  description = "GCloud Region"
}

output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = google_container_cluster.primary.endpoint
  description = "GKE Cluster Host"
}

output "network_project_id" {
  value       = var.network_project_id
  description = "GCloud network Project ID"
}

output "kubernetes_node_pool_version" {
  value       = google_container_node_pool.primary_nodes.version
  description = "Version GKE Node Pool"
}

