# ###GKE cluster
resource "google_container_cluster" "primary" {
  name                      = var.cluster_name
  project                   = var.project_id
  location                  = var.regiao
  remove_default_node_pool  = true
  initial_node_count        = 1
  default_max_pods_per_node = 32
  min_master_version = var.cluster_version
  network    = "projects/${var.network_project_id}/global/networks/${var.vpc_network_name}"
  subnetwork = "projects/${var.network_project_id}/regions/${var.regiao}/subnetworks/${var.vpc_subnet_name}"
  node_locations = [
    "us-central1-a",
    "us-central1-b"
    #"us-central1-f",
  ]
 

  resource_labels = {
    "name" = var.project_id,
    
  }
  addons_config {

    dns_cache_config {
      enabled = false
    }

    gce_persistent_disk_csi_driver_config {
      enabled = true
    }

    horizontal_pod_autoscaling {
      disabled = false
    }

    http_load_balancing {
      disabled = false
    }

    network_policy_config {
      disabled = false
    }
  }

  cluster_autoscaling {
    enabled = false
  }

  default_snat_status {
    disabled = true
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = var.ip_range_pods
    services_secondary_range_name = var.ip_range_services
  }


  maintenance_policy {
    recurring_window {
      end_time   = "2023-01-31T08:00:00Z"
      recurrence = "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU"
      start_time = "2023-01-31T03:00:00Z"
    }
  }

  
  #master_authorized_networks_config {
  #     cidr_blocks {
  #          cidr_block = "10.0.0.0/8"
  #          display_name = "rede-local"
  #      }
  #}

  network_policy {
    enabled  = true
    provider = "CALICO"
  }

  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = true
    master_ipv4_cidr_block  = var.master_ipv4_cidr_block

    master_global_access_config {
      enabled = true
    }
  }
  
  
  release_channel {
    channel = "UNSPECIFIED"
  }

  timeouts {
    update = "10m"
  }

    vertical_pod_autoscaling {
    enabled = true
  }
}

