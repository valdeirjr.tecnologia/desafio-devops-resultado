################################# Variables Comum #######################
variable "project_id" {
  type = string
}

variable "network_project_id" {
  type = string
}
############################ Variables Container Cluster #######################
variable "cluster_name" {
  type = string
}

variable "regiao" {
  type = string

}

variable "zona" {
  type = string

}

variable "descricao" {
  type = string

}

############################ Variables Network #######################
variable "vpc_subnet_name" {
  type = string

}

variable "vpc_network_name" {
  type = string

}

variable "ip_range_pods" {
  type = string

}

variable "ip_range_services" {
  type = string

}

############################ Variables Node_Pool #######################

variable "pool_name" {
  type = string
  default = "pool-1"
}

variable "tipo_maquina" {
  type = string
}

variable "gke_num_nodes" {

  description = "number of gke nodes"
}





variable "service_account" {
  type = string

}


variable "master_ipv4_cidr_block" {
  type    = string
  
}

variable "cluster_version" {
  type    = string
  default = "1.27.2-gke.1200"
  
}
#variable "gke_username" {
#  default     = ""
#  description = "gke username"
#}

#variable "gke_password" {
# default     = ""
#  description = "gke password"
#}
