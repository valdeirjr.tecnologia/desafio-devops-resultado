resource "google_container_node_pool" "primary_nodes" {
  
  max_pods_per_node = 32
  location          = var.regiao
  name              = var.pool_name
  node_count = 1
  node_locations = [
     "us-central1-a",
     "us-central1-b"
    ##"us-central1-f",
  ]
  cluster = google_container_cluster.primary.id
  version = var.cluster_version


  node_config {
    disk_size_gb = 70
    disk_type    = "pd-standard"
    image_type   = "COS_CONTAINERD"
    labels = {
      cluster_name = var.cluster_name,
      node_pool    = var.pool_name
    }
    machine_type = var.tipo_maquina
    metadata = {
      "disable-legacy-endpoints" = "true"
    }
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",

    ]
    preemptible     = false
    service_account = var.service_account
    spot            = false


    tags = [
      "${var.project_id}-gke"
    ]


    shielded_instance_config {
      enable_integrity_monitoring = true
      enable_secure_boot          = false
    }
  }

  upgrade_settings {
    max_surge       = 1
    max_unavailable = 0
   
  }

  autoscaling {
    min_node_count = 1
    max_node_count = 4
    
  }

  timeouts {
    update = "10m"
  }

  management {

    auto_repair  = true
    auto_upgrade = true
  }

}