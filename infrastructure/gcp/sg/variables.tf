variable "project_id" {
  type        = string
  description = "Variável responsável pelo ID do Projeto da GCP."
}

variable "environment" {
  type        = string
  description = "Ambiente que será executado o Terraform"
}

variable "zone" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}

variable "region" {
  type        = string
  description = "Variável da zona de Disponibilidade "
}

variable "bucket_name" {
  type = string
  description = "nome o bucket"

}

variable "storage_class" {
  type = string
  description = "tipo do storage"
}
#variable "subnetwork" {
#  type        = string
#  description = "Valor a subrede a ser configurada na instância."
#}
