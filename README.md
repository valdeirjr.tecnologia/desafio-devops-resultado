# desafio-devops-resultado
Segue o resultado do  **desafios** da segunda fase do processo seletivo para os candidatos da ordem Jedi da **Let’s Code by Ada** para às vagas com o **time de DevOps**. 


## Informação


[Linkedin ](https://www.linkedin.com/in/valdeir-junior-151a7334/)

 [Curriculo](https://storage.cloud.google.com/st-desafio-devops/cv_valdeir_junior.pdf) 

```
cd existing_repo
git remote add origin https://gitlab.com/valdeirjr.tecnologia/desafio-devops-resultado.git
git branch -M main
git push -uf origin main
```
## Usuário no banco
![Tabela Usuário](img/banco.PNG)


 **Extras (diferencial):**

* utilizar HELM;
    - Não foi utilizado.
* divisão de recursos por namespaces;
    - Foram configurados 2 namespace
* utilização de health check na aplicação;
    - Foi implementado /healthcheck
* realizar o deploy através de um pipeline CI/CD;
    - Criado 
* montar um observability mínima da infraestrutura com uma ferramenta de sua escolha;
    - Foi utilizado o prometheus que é ativado na implementação do GKE, e a propia observabilidade disponivel no GKE.
* fazer com que a aplicação exiba seu nome.
    - Não consegui logar na aplicação deu um erro de authenticação no backend
* Foi utilizado 2 formas de fazer deploy no GKE.
    - Via agente
    - Via usuario de serviço